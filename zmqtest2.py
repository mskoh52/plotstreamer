import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np
import sys
import time
import base64
from io import BytesIO
from plotstreamer import Channel

pub_port = 'tcp://127.0.0.1:10000'
sub_port = 'tcp://127.0.0.1:10002'

channel = sys.argv[1]
plottype = sys.argv[2]

with Channel(channel, pub_port, sub_port) as c:
    def sendfigure():
        out = BytesIO()
        fig, ax = plt.subplots(1,1)
        if plottype == 'image':
            ax.imshow(np.random.rand(50,50), interpolation='none')
            fig.set_size_inches(8,8)
            fig.savefig(out, dpi=100)
        else:
            ax.plot(np.random.rand(10, 5))
            fig.set_size_inches(8, 5)
            fig.savefig(out, dpi=100)
        c.send_data(base64.b64encode(out.getvalue()))
        out.seek(0)
        plt.close(fig)

    while True:
        sendfigure()
        time.sleep(1)
