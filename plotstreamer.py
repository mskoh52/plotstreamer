import base64
import time
import threading
import zmq
from io import BytesIO
from queue import Queue, Empty

class Channel():
    def __init__(self,
                 name,
                 pub_port='tcp://127.0.0.1:10000',
                 sub_port='tcp://127.0.0.1:10002'):
        self.name = name
        self.pub_port = pub_port
        self.sub_port = sub_port

        self.event = threading.Event()
        self.pub_q = Queue()

        def _listen():
            zmq_context = zmq.Context.instance()

            with zmq_context.socket(zmq.PUB) as zmq_pub, \
                    zmq_context.socket(zmq.SUB) as zmq_sub:
                zmq_pub.connect(self.pub_port)
                time.sleep(.01)
                zmq_pub.send_string('info', zmq.SNDMORE)
                zmq_pub.send_string('connected\t{0}'.format(self.name))

                zmq_sub.setsockopt(zmq.SUBSCRIBE, b'')
                zmq_sub.connect(self.sub_port)
                poller = zmq.Poller()
                poller.register(zmq_sub, zmq.POLLIN)

                while not self.event.is_set() or not self.pub_q.empty():
                    if not self.pub_q.empty():
                        hdr, data = self.pub_q.get(block=False)
                        print('sending data')
                        zmq_pub.send_string(hdr, zmq.SNDMORE)
                        zmq_pub.send(data)
                    poll = dict(poller.poll(0.01))
                    if zmq_sub in poll:
                        zmq_sub.recv_string()
                        print('Ping received. Sending info')
                        zmq_pub.send_string('info', zmq.SNDMORE);
                        zmq_pub.send_string('connected\t{0}'.format(self.name));

        t = threading.Thread(target=_listen)
        t.start()

    def send_data(self, data):
        self.pub_q.put((self.name, data))

    def send_disconnect(self):
        self.pub_q.put(('info', bytes('disconnected\t{0}'.format(self.name), encoding='ascii')))

    def cleanup(self):
        self.send_disconnect()
        self.event.set()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.cleanup()

class MatplotlibChannel(Channel):
    def sendfigure(self, fig):
        out = BytesIO()
        fig.savefig(out, dpi=100)
        self.send_data(base64.b64encode(out.getvalue()))
        out.seek(0)

