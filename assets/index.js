var socket = io('/index');
var sockid;
var channels = [];
var clicked_channel;

// ===== $(document).ready =====
$(function () {
    socket.on('msg', (msg) => {
        console.log(msg)
    });
    socket.on('sockid', (msg) => {
        sockid = msg.toString();
    });
    socket.on('channels', (msg) => {
        console.log('channels: '+msg);
        channels = msg.sort();
        $('#channel_list').empty();
        for (var i = 0; i < channels.length; i++) {
            $('#channel_list').append('<li class="channel"><a href="plotstreamer" class="channel">'+channels[i]+'</a></li>');
        }
    });

    $("#channel_list").click('a.channel' , (event) => {
        console.log($(event.target).text());
        clicked_channel = $(event.target).text();
        window.open('plotstreamer')
        return false;
    });
});
