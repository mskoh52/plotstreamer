var socket = io('/plotstreamer');
var sockid;
var current_channel;

// ===== Helper functions =====
function resizeCanvas() {
    $('.img').each( function (iter) {
        var parent = $( this ).parent();
        this.setAttribute('width', parent.width());
        this.setAttribute('height', parent.height());
    });
};

function selectChannel(channel) {
    socket.emit('switch', sockid+'\t'+channel);
}

var img = new Image();
var imgdraw = function() {
    var imgdata_cache;
    return function (imgdata) {
        if (!(typeof imgdata === 'undefined')) {
            imgdata_cache = imgdata;
        }
        img.src = 'data:image/png;base64,'+imgdata_cache;
        img.onload = function () {
            var canvas0;
            $('.img').each( function (i) {
                if (i === 0) {
                    canvas0 = this
                    canvas0.getContext('2d').drawImage(img,0,0);
                } else {
                    this.getContext('2d').drawImage(canvas0,0,0);
                }
            });
        };
    };
}();

// ===== $(document).ready =====
$(function(){
    // initialize canvas to correct height
    resizeCanvas();
    current_channel = window.opener.clicked_channel;

    // socket listeners
    socket.on('sockid', (msg) => {
        sockid = msg.toString();
    });
    socket.on('img', (imgdata) => {
        console.log(imgdata)
        imgdraw(imgdata);
    });
    socket.on('msg', (msg) => {
        console.log(msg)
    });
    socket.on('channels', (channels) => {
        channels = channels.sort();
        $('#selector_menu').empty();
        for (var i = 0; i < channels.length; i++) {
            $('#selector_menu').append('<option value="'+channels[i]+'">'+channels[i]+'</option>');
        }
        $('#selector_menu').val(current_channel);
        selectChannel(current_channel);
    });

    // handle selector form
    $('#selector').change((event) => {
        current_channel = $('#selector_menu').val();
        selectChannel(current_channel);
        event.preventDefault();
    });

});

// ===== Update canvas on resize =====
$(window).resize(function() {
    resizeCanvas();
    setTimeout(imgdraw, 0);
});
