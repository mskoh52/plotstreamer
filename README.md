# Plotstreamer v0.3

Stream your plots

## Usage

Start the server:

    user@server$ node server.js

In python:

    from plotstreamer import Channel
    import base64
    from io import BytesIO

    pub_port = 'tcp://127.0.0.1:10000'
    sub_port = 'tcp://127.0.0.1:10002'

    with Channel(channel, pub_port, sub_port) as c:
        def sendfigure():
            out = BytesIO()

            fig, ax = plt.subplots(1,1)
            ax.plot(np.random.rand(10, 5))
            fig.set_size_inches(8, 5)
            fig.savefig(out, dpi=100)

            c.send_data(base64.b64encode(out.getvalue()))
            out.seek(0)

            plt.close(fig)

    while True:
        sendfigure()
        time.sleep(1)
