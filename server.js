var path = require('path');
var fs = require('fs');
var zmq = require('zmq');

// ===== Helper functions =====
function init_socket(socket) {
    zmqping.send('ping');
    socket.emit('sockid', socket.id);
    clients[socket.id] = socket;
    socket.on('disconnect', function(){
        if (clients[socket.id].hasOwnProperty('zmqsock')) {
            clients[socket.id].zmqsock.close();
        }
        delete clients[socket.id];
    });
}

// ===== Set up server =====
var express = require('express');
var app = express();
var http = require('http').createServer(app)
var io = require('socket.io')(http);

app.use('/', express.static(__dirname));
app.use('/res', express.static(path.join(__dirname,  '/assets')));
app.get('/', function (req, res) {
    res.sendFile(path.resolve(__dirname+'/index.html'));
});
app.get('/plotstreamer', function (req, res) {
    res.sendFile(path.resolve(__dirname+'/plotstreamer.html'));
});

// ===== Set up zeromq =====
data_xsub_port = 'tcp://127.0.0.1:10000'
data_xpub_port = 'tcp://127.0.0.1:10001'
ping_pub_port = 'tcp://127.0.0.1:10002'

// zmqxsub and zmqxpub distribute messages from publishers to subscribers
zmqxsub = zmq.socket('xsub');
zmqxsub.bind(data_xsub_port);
zmqxpub = zmq.socket('xpub');
zmqxpub.bind(data_xpub_port);
// zmqping checks for connected publishers
zmqping = zmq.socket('pub');
zmqping.bind(ping_pub_port);
// zmqinfo subscribes to 'info' and listens for responses to pings
zmqinfo = zmq.socket('sub');
zmqinfo.subscribe('info');
zmqinfo.connect(data_xpub_port)

// forward messages between xsub and xpub
zmqxsub.on('message', function (hdr, msg) {
    zmqxpub.send([hdr,msg]);
});
zmqxpub.on('message', function (msg) {
    zmqxsub.send(msg);
});

// handle info sent by publishers in response to pings
var channels = [];
zmqinfo.on('message', function (hdr, msg) {
    var re = /([a-z]*)\t(.*)/.exec(msg.toString());
    type = re[1];
    channel = re[2];
    if ((type === 'connected') && (channels.indexOf(channel) < 0)) {
        channels.push(channel)
    } else if (type === 'disconnected') {
        channels.splice(channels.indexOf(channel), 1);
    }
    for (var c in clients) {
        clients[c].emit('channels', channels);
    }
});

// ===== Set up sockets =====
var clients = {}

var index_nsp = io.of('/index');
index_nsp.on('connection', (socket) => {
    init_socket(socket);
    console.log(socket.id+' joining namespace '+socket.nsp.name);
});


var plotstreamer_nsp = io.of('/plotstreamer');
plotstreamer_nsp.on('connection', (socket) => {
    init_socket(socket);
    console.log(socket.id+' joining namespace '+socket.nsp.name);

    clients[socket.id].zmqsock = new zmq.socket('sub');
    clients[socket.id].zmqsock.connect(data_xpub_port);

    if (channels.length >= 1) {
        clients[socket.id].zmqsock.subscribe(channels[0]);
        clients[socket.id].current_channel = channels[0];
    } else {
        clients[socket.id].zmqsock.subscribe('');
        clients[socket.id].current_channel = '';
    }

    clients[socket.id].zmqsock.on('message', function (hdr,msg) {
        //console.log(socket.id+'>>'+hdr.toString()+':'+msg.toString());
        socket.emit('img', msg.toString());
    });

    socket.on('switch', function(msg){
        re = /(.*)\t(.*)/.exec(msg.toString());
        sid = re[1];
        channel = re[2];
        previous_channel = clients[sid].current_channel;
        console.log(sid +' previous channel: '+ previous_channel);
        clients[sid].zmqsock.unsubscribe(previous_channel);
        clients[sid].current_channel = channel;
        clients[sid].zmqsock.subscribe(channel);
        console.log(sid +' new channel: '+ clients[sid].current_channel);
    });
});

// ===== Start server =====
http.listen(8080, function(){});

// ===== Cleanup =====
/*
process.on('exit', exitHandler);
process.on('SIGINT', exitHandler);

function exitHandler() {
    zmqsock.close();
    process.exit();
}
*/
